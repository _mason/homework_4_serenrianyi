package com;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class TestMain {

    @Test
    public void Testqsort() throws IOException {
        Main main = new Main();
        int[] result = main.qsort(new int[]{5, 2, 7, 2, 4, 8, 2});

        Assert.assertArrayEquals(new int[]{2, 2, 2, 4, 5, 7, 8},result);
    }

    @Test
    public void Testpartition(){
        Main main = new Main();
        int[] mass = {6,2,5,3,1,4};
        int pivot = main.partition(mass,0,mass.length-1);

        Assert.assertEquals(3,pivot);

        for (int i = 0; i < pivot; i++) {
            Assert.assertTrue(mass[i] <= mass[pivot]);
        }

        for (int i = pivot; i < mass.length; i++) {
            Assert.assertTrue(mass[i] >= mass[pivot]);
        }


    }
}
