package com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

public class Main {
    public static int ArrayLength = 30;
    public static int[] array = new int[ArrayLength];
    public static Random generator = new Random();
    public static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


    public static void main(String[] args) throws IOException {
        ArrGenerate();
        ArrPrint();
        System.out.println("");
        qsort(array);
        ArrPrint();
        System.out.println("");
        ArrGenerate();
//        Arrays.toString(array)
        ArrPrint();
        System.out.println("\nВведите индексы элементов");
        int from = Integer.parseInt(reader.readLine());
        int to = Integer.parseInt(reader.readLine());
        while (from>to || from<0 || to >29){
            System.out.println("Не верные индексы");
            from = Integer.parseInt(reader.readLine());
            to = Integer.parseInt(reader.readLine());
        }
        FromToqsort(array, from, to);
        ArrPrint();
    }

    public static void ArrGenerate(){
        for (int i = 0; i < ArrayLength ; i++) {
            array[i] = generator.nextInt(100);
        }
    }

    public static void ArrPrint(){
        for (int i = 0; i < ArrayLength ; i++) {
            System.out.print(array[i] + ", ");
        }
    }

    public static int[] qsort(int[] array) throws IOException {
        FromToqsort(array,0,array.length-1);
        return array;
    }

    public static void FromToqsort(int[] array, int from, int to) throws IOException {
        if ( from >= to)
        {
            return;
        }
        int pivot = partition (array, from, to);
        FromToqsort (array, from, pivot-1);
        FromToqsort (array, pivot+1, to);
    }

    public static int partition(int[] array, int from, int to){
        int marker = from;
        for ( int i = from; i <= to; i++ )
        {
            if ( array[i] <= array[to] )
            {
                int temp = array[marker];
                array[marker] = array[i];
                array[i] = temp;
                marker += 1;
            }
        }
        return marker - 1;
    }
}